resource "random_password" "demo_password" {
  count            = "3"
  length           = 11
  special          = true
  override_special = "%&#$_"
}

resource "azurerm_network_interface" "demo_inc" {
  count               = "3"
  name                = "nic-${count.index}"
  location            = azurerm_resource_group.demo.location
  resource_group_name = azurerm_resource_group.demo.name

  ip_configuration {
    name                          = "ipconfig-${count.index}"
    subnet_id                     = azurerm_subnet.demo_subnet.id
    private_ip_address_allocation = "Dynamic"
  }

  tags = var.tags

}


resource "azurerm_linux_virtual_machine" "demo_linux" {
  count                           = "3"
  name                            = "vm-${count.index}"
  resource_group_name             = azurerm_resource_group.demo.name
  location                        = azurerm_resource_group.demo.location
  zone                            = element(var.availability_zones, count.index)
  size                            = "Standard_F2"
  admin_username                  = "azureuser"
  admin_password                  = element(random_password.demo_password.*.result, count.index)
  disable_password_authentication = false
  network_interface_ids           = [
    element(azurerm_network_interface.demo_inc.*.id, count.index),
    ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"

  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update -y",
      "sudo apt install apache2 -y",
      "sudo systemctl start apache2",
      "sudo systemctl enable apache2",
      "echo <p>This is VM-${count.index + 1}</p> > /var/www/html/index.html",
      "sudo systemctl reload apache2"
    ]
    on_failure = continue
  }

  connection {
    host             = element(azurerm_linux_virtual_machine.demo_linux.*.private_ip_addresses, count.index)
    bastion_user     = "azureuser"
    bastion_password = element(random_password.demo_password.*.result, count.index)
    bastion_host     = azurerm_bastion_host.bastion_host.dns_name
  }

  tags = var.tags

}